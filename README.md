# aiven-deployment-jenkins

Repositories:

    1. Backend - https://gitlab.com/cloud-listing/atlas.git
    2. Frontend - https://gitlab.com/cloud-listing/picasso.git
    3. Deployment - https://gitlab.com/cloud-listing/deployment.git


Steps to run application:

Using Docker

    1. Clone repository https://gitlab.com/cloud-listing/deployment.git
    2. Run docker-compose -f /path/to/docker-compose.yml up -d
    3. Access frontend from http://localhost:3000

Manual installation:

    Backend:
    1. Clone repository https://gitlab.com/cloud-listing/atlas.git which has backend app.
    2. Create virtual environment and install dependencies from requirements.txt file
    3. python3 server.py

    Frontend:
    4. Clone repository https://gitlab.com/cloud-listing/picasso.git
    5. npm start

Using Jenkins with CI:

    1. The Jenkinsfile is present in repository https://gitlab.com/cloud-listing/deployment.git which pulls the code from Github, builds the docker images and deploy it in a remote server
    2. Install Publish over SSH plugin in Jenkins
    3. Configure remote host credentials with private key in Jenkins environment as web-host 
    4. Create a pipeling by using the Jenkinsfile from repo
